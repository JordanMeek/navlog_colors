Jan 18th, 2018
==============
* Changed all headers to #BECAD4 @ 10px.
* Start second line with "Block Fuel".

Jan 19th, 2018
==============
* Slightly increased the size of the top header text.
* Made the header text color change for dark mode only.